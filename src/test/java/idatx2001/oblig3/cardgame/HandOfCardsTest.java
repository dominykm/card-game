package idatx2001.oblig3.cardgame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {

    @Test
    @DisplayName("Test to add a card")
    void addCard(){
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('D', 12));
        handOfCards.addCard(new PlayingCard('D', 12));
        handOfCards.addCard(new PlayingCard('D', 11));
        assertTrue(handOfCards.getHandOfCards().size() == 2);
    }

    @Test
    @DisplayName("Test when hand of cards is a 5 card flush")
    void flushWith5CardsTrue(){
        HandOfCards hand = new HandOfCards();
        for(int i =0; i<5; i++){
            hand.addCard(new PlayingCard('D', i));
        }
        assertTrue(hand.hasFlush(5));
    }

    @Test
    @DisplayName("Test when hand of cards is not a flush")
    void flushWith5CardsFalse(){
        HandOfCards hand = new HandOfCards();
        hand.addCard(new PlayingCard('C', 1));
        for(int i =0; i<4; i++){
            hand.addCard(new PlayingCard('D', i));
        }
        assertFalse(hand.hasFlush(5));
    }

    @Test
    @DisplayName("Test when hand of cards is a 6 card flush")
    void flushWith6CardsTrue(){
        HandOfCards hand = new HandOfCards();
        for(int i =0; i<6; i++){
            hand.addCard(new PlayingCard('D', i));
        }
        assertTrue(hand.hasFlush(5));
    }

}