package idatx2001.oblig3.cardgame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

    @Test
    @DisplayName("Creates new deck")
    void createNewDeck(){
        DeckOfCards deck = new DeckOfCards();

        assertTrue(deck.getPlayingCards().size() == 52);
    }

    @Test
    @DisplayName("Creates new hand of 5 cards")
    void createNewHandOfCards(){
        DeckOfCards deck = new DeckOfCards();
        HandOfCards handOfCards= deck.dealHand(5);

        assertTrue(handOfCards.getHandOfCards().size() == 5);
    }


}