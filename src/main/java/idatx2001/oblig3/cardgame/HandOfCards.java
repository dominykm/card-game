package idatx2001.oblig3.cardgame;

import java.util.ArrayList;

/**
 * represents a hand of cards. Hand of cards is a list of n amount of cards.
 * All cards in the hand are different
 */

public class HandOfCards {
    private ArrayList<PlayingCard> handOfCards = new ArrayList<>();

    public ArrayList<PlayingCard> getHandOfCards() {
        return handOfCards;
    }

    /**
     * method to add a card to the hand of cards list.
     * Checks if the card is the list from before
     * @param card a new playing card to be added to the list
     * @return returns true if the cards was added, false if not
     */
    public boolean addCard(PlayingCard card){
        if(!handOfCards.contains(card)){
            handOfCards.add(card);
            return true;
        }
        return false;
    }

    /**
     * checks if a hand of cards has a flush or not.
     * A flush is if the hand has n amount of the same suit cards.
     * @param amountToFlush the number of same suit cards needed to get a flush
     * @return returns true if there is a flush in the hand, false if there is no flush
     */
    public boolean hasFlush(int amountToFlush){
        long handOfS = handOfCards.stream().filter(c->c.getSuit()=='S').count();
        long handOfH = handOfCards.stream().filter(c->c.getSuit()=='H').count();
        long handOfC = handOfCards.stream().filter(c->c.getSuit()=='C').count();
        long handOfD = handOfCards.stream().filter(c->c.getSuit()=='D').count();
        if(handOfS >= amountToFlush ||handOfH >= amountToFlush ||handOfC >= amountToFlush ||handOfD >= amountToFlush){
            return true;
        }
        return false;
    }
}
