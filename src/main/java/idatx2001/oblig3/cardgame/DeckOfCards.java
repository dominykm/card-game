package idatx2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Random;

/**
 * Represents a deck of 52 cards. A card is represented by a suit and a face.
 * The possible suits are 'S', 'H', 'D' and 'C' and possible faces are 1 to 13.
 */
public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private ArrayList<PlayingCard> playingCards = new ArrayList<>();

    /**
     * constructor that creates a deck of 52 cards
     */
    public DeckOfCards(){
        for(char suit: suit){
            for(int i=1; i<=13; i++){
                playingCards.add(new PlayingCard(suit,i));
            }
        }
    }

    public ArrayList<PlayingCard> getPlayingCards() {
        return playingCards;
    }

    /**
     * deals a hand to the player with n amount of cards
     * Method chooses n amount of random cards from the deck that are not in the hand from before
     * @param n the amount of cards to be dealt to the hand
     * @return a list over cards in the hand
     */
    public HandOfCards dealHand(int n){
        HandOfCards handOfCards = new HandOfCards();
        int i=0;
        while(i<n){
            Random r = new Random();
            int randomSlot= r.nextInt(52);
           if(!handOfCards.getHandOfCards().contains(playingCards.get(randomSlot))){
               handOfCards.addCard(playingCards.get(randomSlot));
               i++;
           }
        }
        return handOfCards;
    }
}
