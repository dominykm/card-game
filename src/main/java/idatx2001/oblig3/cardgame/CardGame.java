package idatx2001.oblig3.cardgame;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;



public class CardGame extends Application {
    HandOfCards handOfCards;
    ArrayList<PlayingCard> handOfCardsList;
    String handOfCardsString="";
    String heartCardListString ="";

    Text sumAnswer = new Text();
    Text heartsAnswer = new Text();
    Text flushAnswer = new Text();
    Text queenAnswer = new Text();

    TextField amountToFlush = new TextField("5");
    TextField amountInHand = new TextField("5");
    int amountInHandInt;
    int amountToFlushInt;

    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception {

        DeckOfCards deckOfCards = new DeckOfCards();

        primaryStage.setTitle("Card Game oblig 3, idatt2001");

        //Grid layout
        GridPane gp = new GridPane();
        gp.setMinSize(700,400);
        gp.setPadding(new Insets(60,10,10,60));
        gp.setHgap(20);
        gp.setVgap(40);

        //StackPanes
        StackPane sp = new StackPane();
        sp.setStyle("-fx-border-color: black");
        sp.setMinSize(300,200);


        //Boxes around the answers
        HBox sumBox = new HBox();
        sumBox.setStyle("-fx-border-color: black");
        sumBox.setMinSize(30, 20);
        HBox cardOfHeartsBox = new HBox();
        cardOfHeartsBox.setStyle("-fx-border-color:black");
        cardOfHeartsBox.setMinSize(30,20);
        HBox flushBox = new HBox();
        flushBox.setStyle("-fx-border-color:black");
        flushBox.setMinSize(30,20);
        HBox queenBox = new HBox();
        queenBox.setStyle("-fx-border-color:black");
        queenBox.setMinSize(30,20);

        //Buttons
        Button dealHand = new Button("Deal hand");
        Button checkHand = new Button("Check hand");
        //Labels
        Label cardsDisplay = new Label("Cards here");
        amountInHand.setPrefWidth(50);
        amountToFlush.setPrefWidth(50);

        //When deal hand is pressed
        dealHand.setOnAction(actionEvent ->{
            resetTextVariables();
            if(isNumber()) {
                handOfCardsString = "";
                handOfCards = deckOfCards.dealHand(amountInHandInt);
                handOfCardsList = handOfCards.getHandOfCards();
                handOfCardsList.forEach(c -> handOfCardsString += c.getAsString() + " ");
            }
            cardsDisplay.setText(handOfCardsString);
        });

        //Check hand button
        checkHand.setOnAction(actionEvent -> {
            resetTextVariables();
            if(isNumber()) {
                if (handOfCards.hasFlush(amountToFlushInt)) {
                    flushAnswer.setText("Yes");
                } else {
                    flushAnswer.setText("No");
                }
                //sums all the faces
                sumAnswer.setText(handOfCardsList.stream().map(PlayingCard::getFace).reduce((a, b) -> a + b).get().toString());
                //list all heart cards from hand of cards
                handOfCardsList.stream().filter(c -> c.getSuit() == 'H').forEach(h -> heartCardListString += h.getAsString() + " ");
                heartsAnswer.setText(heartCardListString);
                if (heartCardListString.isBlank()) {
                    heartsAnswer.setText("No hearts");
                }
                //Checks if there is a queen of spades in the hand of cards
                if (handOfCardsList.contains(new PlayingCard('S', 12))) {
                    queenAnswer.setText("Yes");
                } else {
                    queenAnswer.setText("No");
                }
            }
        });


        sp.getChildren().add(cardsDisplay);
        gp.add(sp, 0,1,5,6);
        gp.add(flushBox,7,5,3,1);
        gp.add(cardOfHeartsBox,7,3,3,1);
        gp.add(sumBox,7,2,3,1);
        gp.add(queenBox,7,4,3,1);

        gp.add(checkHand,3,8);
        gp.add(dealHand,2,8);

        gp.add(new Label("Amount of cards to flush:"),3,7);
        gp.add(new Label("Amount of cards in hand:"),1,7);
        gp.add(amountInHand,2,7);
        gp.add(amountToFlush,4,7);
        gp.add(new Label("Sum of the faces: "),6,2);
        gp.add(sumAnswer,8,2);
        GridPane.setHalignment(sumAnswer, HPos.CENTER);
        gp.add(new Label("Cards of Heart: "),6,3);
        gp.add(heartsAnswer,8,3);
        GridPane.setHalignment(queenAnswer, HPos.CENTER);
        gp.add(new Label("Queen of spades: "),6,4);
        gp.add(queenAnswer,8,4);
        gp.add(new Label("Flush: "),6,5);
        gp.add(flushAnswer,8,5);
        GridPane.setHalignment(flushAnswer, HPos.CENTER);
        primaryStage.setScene(new Scene(gp,1000,600));
        primaryStage.show();


    }

    /**
     * resets answer text variables to blank
     */
    private void resetTextVariables(){
        heartCardListString = "";
        flushAnswer.setText("");
        heartsAnswer.setText("");
        sumAnswer.setText("");
        queenAnswer.setText("");
    }

    /**
     * checks if amount in hand and amount to flush fields are integers
     * and if the fields are not blank
     * @return returns true if the variables are parsed to integer
     * returns false if variables are not parsed to integer or they are blank
     */
    private boolean isNumber(){
        if(amountInHand.getText().isBlank()||amountToFlush.getText().isBlank()){
            return false;
        }
        try{
            amountInHandInt = Integer.parseInt(amountInHand.getText());
            amountToFlushInt = Integer.parseInt(amountToFlush.getText());
            return true;
        }catch (NumberFormatException n){
            handOfCardsString = "Amount to flush or/and amount in hand number field is either blank or wrong input";
            handOfCardsList.clear();
        }
        return false;
    }
}
